package pt.velv.galpexpand.tests;

import org.junit.Test;
import org.mongodb.morphia.query.Query;

import pt.velv.galpexpand.db.connection.MongoConnection;
import pt.velv.galpexpand.db.dao.SurveyDataDAO;
import pt.velv.galpexpand.model.survey.Survey;

public class DBTestIT {

	@Test
	public void testGetArchetype() {
		final MongoConnection conn = MongoConnection.getInstance();
		final SurveyDataDAO dao = new SurveyDataDAO(conn.getDatastore());
		final Query<Survey> query = dao.createQuery();
		query.and(query.criteria("companyId").equal("GalpExpand"), query.criteria("language").equal("EN"));
		Survey surveyData = query.get();
		System.out.println(surveyData.getLanguage());
		
	}

	// @Test
	// public void testGetSurvey() {
	// MongoConnection conn = MongoConnection.getInstance();
	// SurveyDataDAO dao = new SurveyDataDAO(conn.getDatastore());
	// ConcurrentHashMap<String, String> searchValues = new ConcurrentHashMap<>();
	// searchValues.put("companyId", "GALPEXPAND_TEST");
	// searchValues.put("language", "PT");
	//
	// Query<Survey> query = dao.createQuery();
	// searchValues.forEach((k, v) -> query.field(k).equal(searchValues.get(k)));
	//
	// Survey result = query.get();
	// System.out.println(result.getSurveyId());
	// }
	//
	// @Test
	// public void testGetUserData() {
	// MongoConnection conn = MongoConnection.getInstance();
	// UserDataDAO dao = new UserDataDAO(conn.getDatastore());
	// ConcurrentHashMap<String, String> searchValues = new ConcurrentHashMap<>();
	// searchValues.put("userId", "jjesus");
	// searchValues.put("companyId", "GALPEXPAND");
	//
	// Query<UserData> query = dao.createQuery();
	//// searchValues.forEach((k, v) -> query.field(k).equal(searchValues.get(k)));
	// query.field("userId").equal("jjesus").field("companyId").equals("GALPEXPAND");
	//
	// UserData result = query.get();
	// System.out.println("userId:" + result.getUserId());
	//
	// }
	//// @Test
	// public void testSurveyResultsRead () {
	// MongoConnection conn = MongoConnection.getInstance();
	// UserDataDAO dao = new UserDataDAO(conn.getDatastore());
	//
	// UserData result = dao.findOne("user", "user@email.com");
	// System.out.println("Size:" +
	// result.getAnswers().get(0).getOptions().get(0).getAnswer());
	// }
	//
	//// @Test
	// public void testSurveyDataCreation() {
	// MongoConnection conn = MongoConnection.getInstance();
	// SurveyDataDAO dao = new SurveyDataDAO(conn.getDatastore());
	//
	// dao.save(buildSurveyDataObject("GALPEXPAND_TEST"));
	//
	// Survey result = dao.findOne("surveyId", "galpExpand");
	//
	// assertNotNull(result);
	// }
	//
	//// @Test
	// public void testUserDataCreation() {
	// MongoConnection conn = MongoConnection.getInstance();
	// UserDataDAO dao = new UserDataDAO(conn.getDatastore());
	//
	//
	// dao.save(buildUserDataObject("TEST_USER"));
	//
	// UserData result = dao.findOne("surveyId", "galpExpand");
	//
	// assertNotNull(result);
	// }
	//
	// public static UserData buildUserDataObject(String userId) {
	// UserData userData = new UserData();
	// userData.setStatus("Completed");
	//// userData.setUserCompany("GALPEXPAND");
	//// userData.setUserMail("TEST@user.com");
	// userData.setUserId("jjesus");
	////
	//// ArchetypeData archetype = new ArchetypeData();
	//// archetype.setId("SHARE");
	//// userData.setArchetype(archetype);
	// return userData;
	// }
	//
	// public static Survey buildSurveyDataObject(String surveyId) {
	//
	// // SURVEYDATA
	// Survey survey = new Survey();
	// survey.setSurveyId(surveyId);
	//
	// // INTRO
	// SurveyIntroData intro = new SurveyIntroData();
	// intro.setCompany("MICOMPANY");
	// intro.setFirstMessage("Na Galp, acreditamos que somos <br> a soma de quem
	// trabalha connosco.");
	// intro.setSecondMessage("Queremos ajud�-lo a atingir<br> o seu potencial");
	// intro.setSecondMessageSbutitle(
	// "Para isso, vamos fazer-lhe umas perguntas simples :)<br> Pode voltar atr�s e
	// alterar quantas vezes quiser.");
	//
	// // QUESTIONS
	// SurveyQuestion question = new SurveyQuestion();
	// question.setStep("1");
	// question.setTemplate("question-two-options-image");
	// question.setTemplateUrl("assets/anim-json/counter-green.json");
	// question.setImageUrl("http://localhost:4200/assets/sample-image.jpg");
	// question.setQuestion("O meu super poder �");
	//
	// List<SurveyOption> options = new ArrayList<SurveyOption>();
	//
	// // Options
	// SurveyOption option_1 = new SurveyOption();
	// option_1.setAnswer("...as rela��es de confian�a que consigo criar, tenho
	// tanto a aprender com cada pessoa");
	// option_1.setArchetype("Share");
	// options.add(option_1);
	// SurveyOption option_2 = new SurveyOption();
	// option_2.setAnswer("�a resili�ncia, nunca me canso de experimentar coisas
	// novas");
	// option_2.setArchetype("Experiment");
	// options.add(option_2);
	//
	// question.setOptions(options);
	//
	// List<SurveyQuestion> questions = new ArrayList<SurveyQuestion>();
	// questions.add(question);
	//
	// SurveyData surveyData = new SurveyData();
	// survey.setSurveyData(surveyData);
	// surveyData.setQuestions(questions);
	// surveyData.setSurveyIntro(intro);
	//
	// return survey;
	// }

}
