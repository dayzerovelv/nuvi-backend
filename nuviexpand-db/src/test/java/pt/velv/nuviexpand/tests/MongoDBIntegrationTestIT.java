package pt.velv.galpexpand.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.mongodb.MongoClient;

import pt.velv.galpexpand.db.connection.MongoConnection;

public class MongoDBIntegrationTestIT {

	@Test
	public void testClientConnectionIT() {
		MongoConnection instance = MongoConnection.getInstance();

		MongoClient client = instance.getMongo();

		try {
			client.getAddress();
		} catch (Exception e) {
			assertFalse(true);
		}
		assertTrue(true);
	}
}