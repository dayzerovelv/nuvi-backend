package pt.velv.galpexpand.db.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import pt.velv.galpexpand.model.survey.Survey;

public class SurveyDataDAO extends BasicDAO<Survey, ObjectId> {

    public SurveyDataDAO(Datastore ds) {
        super(ds);
    }
	
}