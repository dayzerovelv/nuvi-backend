package pt.velv.galpexpand.db;

import java.util.concurrent.ConcurrentHashMap;

import pt.velv.galpexpand.model.archetype.ArchetypeData;
import pt.velv.galpexpand.model.survey.Survey;
import pt.velv.galpexpand.model.user.UserData;

public interface DBService {

//	Survey getSurveyById(String id);

	boolean saveUserdata(UserData userData);

	UserData getUserData(String tokenId, String companyId);

	Survey getSurveyByFields(ConcurrentHashMap<String, String> searchValues);

	ArchetypeData getArchetypeData(String language, String archetype);

}
