package pt.velv.galpexpand.db.connection;

import static java.lang.String.format;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;

import pt.velv.galpexpand.model.survey.Survey;

public class MongoConnection {

	private final Logger logger = LoggerFactory.getLogger(MongoConnection.class);

	private static MongoConnection instance = new MongoConnection();

	private MongoClient mongo = null;
	private Datastore dataStore = null;
	private Morphia morphia = null;

	private MongoConnection() {

	}

	public MongoClient getMongo() throws RuntimeException {
		if (mongo == null) {
			logger.debug("GOOOO");
			/**
			 * TODO : Connection Variables ( Dunno what to do about it now )
			 */
			MongoClientOptions.Builder options = MongoClientOptions.builder().connectionsPerHost(4)
					.maxConnectionIdleTime((60 * 1000)).maxConnectionLifeTime((120 * 1000));

			MongoClientURI uri = new MongoClientURI("mongodb://localhost:27017/surveys", options);

			logger.info("Connecting to MongoDB @ " + uri.toString());

			try {
				mongo = new MongoClient(uri);
			} catch (MongoException ex) {
				logger.error("An error occoured when connecting to MongoDB", ex);
			} catch (Exception ex) {
				logger.error("An error occoured when connecting to MongoDB", ex);
			}

		}

		return mongo;
	}

	public Morphia getMorphia() {
		if (morphia == null) {
			logger.debug("Loading Morphia");
			morphia = new Morphia();
			morphia.mapPackageFromClass(Survey.class);
		}

		return morphia;
	}

	public Datastore getDatastore() {
		if (dataStore == null) {
			String dbName = "surveys";
			logger.debug(format("Starting DataStore on DB: %s", dbName));
			dataStore = getMorphia().createDatastore(getMongo(), dbName);
		}

		return dataStore;
	}

	public void init() {
		getMongo();
		getMorphia();
		getDatastore();
	}

	public void close() {
		logger.info("Closing MongoDB connection");
		if (mongo != null) {
			try {
				mongo.close();
				logger.debug("Nulling the connection dependency objects");
				mongo = null;
				morphia = null;
				dataStore = null;
			} catch (Exception e) {
				logger.error(format("An error occurred when closing the MongoDB connection\n%s", e.getMessage()));
			}
		} else {
			logger.warn("mongo object was null, wouldn't close connection");
		}
	}

	public static MongoConnection getInstance() {
		return instance;
	}
}