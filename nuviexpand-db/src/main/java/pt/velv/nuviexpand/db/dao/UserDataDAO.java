package pt.velv.galpexpand.db.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import pt.velv.galpexpand.model.user.UserData;

public class UserDataDAO extends BasicDAO<UserData, ObjectId> {

	public UserDataDAO(Datastore ds) {
        super(ds);
    }

}
