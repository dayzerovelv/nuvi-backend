package pt.velv.galpexpand.db.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.mongodb.morphia.query.Criteria;
import org.mongodb.morphia.query.CriteriaContainer;
import org.mongodb.morphia.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import pt.velv.galpexpand.db.DBService;
import pt.velv.galpexpand.db.connection.MongoConnection;
import pt.velv.galpexpand.db.dao.ArchetypeDataDAO;
import pt.velv.galpexpand.db.dao.SurveyDataDAO;
import pt.velv.galpexpand.db.dao.UserDataDAO;
import pt.velv.galpexpand.model.archetype.ArchetypeData;
import pt.velv.galpexpand.model.survey.Survey;
import pt.velv.galpexpand.model.user.UserData;

@Service
public class DBServiceImpl implements DBService {

	private final Logger logger = LoggerFactory.getLogger(DBServiceImpl.class);

	@Override
	public UserData getUserData(final String userToken, final String companyId) {
		final MongoConnection conn = MongoConnection.getInstance();
		final UserDataDAO dao = new UserDataDAO(conn.getDatastore());
		final Query<UserData> query = dao.createQuery();
		logger.debug("[MONGODB] :: Returning user data for token {}", userToken);
		query.field("token").equal(userToken);
		return query.get();
	}

	@Override
	public boolean saveUserdata(final UserData userData) {
		final MongoConnection conn = MongoConnection.getInstance();
		final UserDataDAO dao = new UserDataDAO(conn.getDatastore());
		logger.debug("[MONGODB] :: Saving user data for user {}", userData.getUser());
		try {
			dao.delete(userData);
			dao.save(userData);
		} catch (final Exception e) {
			logger.error("[MONGODB] :: FAILED TO SAVE USER DATA {}", userData.toString());
			return false;
		}
		return true;
	}

	@Override
	public Survey getSurveyByFields(final ConcurrentHashMap<String, String> searchValues) {
		final MongoConnection conn = MongoConnection.getInstance();
		final SurveyDataDAO dao = new SurveyDataDAO(conn.getDatastore());
		final Query<Survey> query = dao.createQuery();
		logger.debug("[MONGODB] :: Retrieving survey data.");
		
		List<CriteriaContainer> andClauses = new ArrayList<CriteriaContainer>();
		
		for(Entry<String, String> entry : searchValues.entrySet()) {
			andClauses.add(query.criteria(entry.getKey()).equal(entry.getValue()));
		}
		query.and(andClauses.toArray(new Criteria[andClauses.size()]));

		return query.get();
	}

	@Override
	public ArchetypeData getArchetypeData(final String language, final String archetype) {
		final MongoConnection conn = MongoConnection.getInstance();
		final ArchetypeDataDAO dao = new ArchetypeDataDAO(conn.getDatastore());
		
		final Query<ArchetypeData> query = dao.createQuery();
//		query.field("language").equals(language);
//		query.field("archetype").equals(archetype);
		
		List<CriteriaContainer> andClauses = new ArrayList<CriteriaContainer>();
		andClauses.add(dao.createQuery().criteria("language").equal(language));
		andClauses.add(dao.createQuery().criteria("name").equal(archetype));
		
		query.and(andClauses.toArray(new Criteria[andClauses.size()]));
		
		return query.get();

	}

}
