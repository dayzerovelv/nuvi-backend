package pt.velv.galpexpand.db.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import pt.velv.galpexpand.model.archetype.ArchetypeData;

public class ArchetypeDataDAO extends BasicDAO<ArchetypeData, ObjectId> {

    public ArchetypeDataDAO(final Datastore ds) {
        super(ds);
    }
	
}