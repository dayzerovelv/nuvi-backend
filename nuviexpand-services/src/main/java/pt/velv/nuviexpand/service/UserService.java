package pt.velv.galpexpand.service;

import pt.velv.galpexpand.model.archetype.ArchetypeData;
import pt.velv.galpexpand.model.user.UserData;

public interface UserService {

	boolean saveUserData(UserData userData);

	UserData getUserData(String userId, String companyId);

	ArchetypeData getArcheTypeData(String language, String archetype);
	
}
