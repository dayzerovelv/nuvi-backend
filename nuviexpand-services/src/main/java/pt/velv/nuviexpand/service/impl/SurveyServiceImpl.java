package pt.velv.galpexpand.service.impl;

import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.velv.galpexpand.db.DBService;
import pt.velv.galpexpand.model.survey.Survey;
import pt.velv.galpexpand.service.SurveyService;

@Service
public class SurveyServiceImpl implements SurveyService {
	
	private final Logger logger = LoggerFactory.getLogger(SurveyServiceImpl.class);
	
	@Autowired
	DBService dbService;

	@Override
	public Survey getUserSurveyWithCompany(String companyId, String language) {
		ConcurrentHashMap<String, String> searchValues = new ConcurrentHashMap<>();
		searchValues.put("companyId", companyId);
		searchValues.put("language", language);
		logger.info("[SERVICES - SurveyService] :: Received request for getUserSurveyWithCompany");
		return this.dbService.getSurveyByFields(searchValues);
	}

}
