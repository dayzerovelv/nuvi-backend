package pt.velv.galpexpand.service;

import pt.velv.galpexpand.model.survey.Survey;

public interface SurveyService {

	Survey getUserSurveyWithCompany(String companyId, String language);
	
}
