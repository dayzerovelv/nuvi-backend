package pt.velv.galpexpand.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.velv.galpexpand.db.DBService;
import pt.velv.galpexpand.model.archetype.ArchetypeData;
import pt.velv.galpexpand.model.user.UserData;
import pt.velv.galpexpand.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	DBService dbService;

	@Override
	public UserData getUserData(final String userToken, final String companyId) {
		logger.info("[SERVICES - UserService] :: Received request for getUserData  with token {}", userToken);
		return dbService.getUserData(userToken, companyId);
	}

	@Override
	public boolean saveUserData(final UserData userData) {
		logger.info("[SERVICES - UserService] :: Received request for saveUserData");
		return dbService.saveUserdata(userData);
	}

	@Override
	public ArchetypeData getArcheTypeData(final String language, final String archetype) {
		return dbService.getArchetypeData(language, archetype);
	}

}
