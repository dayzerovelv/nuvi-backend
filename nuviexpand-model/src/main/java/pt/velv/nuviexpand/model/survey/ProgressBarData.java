package pt.velv.galpexpand.model.survey;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

@Embedded
public class ProgressBarData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6672095053814719557L;

	@Property("start")
	private String start;

	@Property("middle")
	private String middle;

	@Property("end")
	private String end;

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getMiddle() {
		return middle;
	}

	public void setMiddle(String middle) {
		this.middle = middle;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}
	
	
}
