package pt.velv.galpexpand.model.survey.questions;

import java.io.Serializable;
import java.util.List;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

import pt.velv.galpexpand.model.survey.QuestionAnswerBlock;

@Embedded
public class SurveyQuestion implements Serializable{

	private static final long serialVersionUID = -5890134952105590620L;

	@Property("step")
	private String step;

	@Property("template")
	private String template;

	@Property("templateUrl")
	private String templateUrl;

	@Property("title")
	private String title;
	
	@Property("description")
	private String description;
	
	@Property("question")
	private String question;

	@Property("image")
	private String image;

	@Embedded
	private List<SurveyOption> options;

	@Embedded
	private List<QuestionAnswerBlock> blocks;

	public String getStep() {
		return step;
	}

	public void setStep(final String step) {
		this.step = step;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(final String template) {
		this.template = template;
	}

	public String getTemplateUrl() {
		return templateUrl;
	}

	public void setTemplateUrl(final String templateUrl) {
		this.templateUrl = templateUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(final String question) {
		this.question = question;
	}

	public String getImage() {
		return image;
	}

	public void setImage(final String image) {
		this.image = image;
	}

	public List<SurveyOption> getOptions() {
		return options;
	}

	public void setOptions(final List<SurveyOption> options) {
		this.options = options;
	}

	public List<QuestionAnswerBlock> getBlocks() {
		return blocks;
	}

	public void setBlocks(final List<QuestionAnswerBlock> blocks) {
		this.blocks = blocks;
	}


}
