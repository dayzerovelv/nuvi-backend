package pt.velv.galpexpand.model.user;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

import pt.velv.galpexpand.model.BaseDomainObject;

@Embedded
public class UserOption implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4782620124773476748L;

	@Property("answer")
	private String answer;
	
	@Property("archetype")
	private String archetype;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getArchetype() {
		return archetype;
	}

	public void setArchetype(String archetype) {
		this.archetype = archetype;
	}

}
