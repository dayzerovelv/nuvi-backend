package pt.velv.galpexpand.model.survey.questions;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

import pt.velv.galpexpand.model.survey.FeedBackData;
import pt.velv.galpexpand.model.survey.OptionsItemData;

@Embedded
public class SurveyOption implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4076995134844966305L;

	@Property("answer")
	private String answer;

	@Property("archetype")
	private String archetype;

	@Property("imageUrl")
	private String imageUrl;

	@Property("imageHoverUrl")
	private String imageHoverUrl;

	@Property("author")
	private String author;

	@Embedded
	private OptionsItemData item;

	@Embedded
	private FeedBackData feedback;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getArchetype() {
		return archetype;
	}

	public void setArchetype(String archetype) {
		this.archetype = archetype;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getImageHoverUrl() {
		return imageHoverUrl;
	}

	public void setImageHoverUrl(String imageHoverUrl) {
		this.imageHoverUrl = imageHoverUrl;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public OptionsItemData getItem() {
		return item;
	}

	public void setItem(OptionsItemData item) {
		this.item = item;
	}

	public FeedBackData getFeedback() {
		return feedback;
	}

	public void setFeedback(FeedBackData feedback) {
		this.feedback = feedback;
	}
	
	

}
