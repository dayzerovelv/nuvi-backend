package pt.velv.galpexpand.model.archetype;

import java.io.Serializable;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

@Entity
public class ArchetypeData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2337506222786852589L;

	@Id
	ObjectId objectId;

	@Property("name")
	private String name;

	@Property("template")
	private String template;
	
	@Property("language")
	private String language;

	@Embedded
	private GenderArchetype male;

	@Embedded
	private GenderArchetype female;

	@Property("words")
	private List<String> words;

	public ObjectId getObjectId() {
		return objectId;
	}

	public void setObjectId(final ObjectId objectId) {
		this.objectId = objectId;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(final String template) {
		this.template = template;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(final String language) {
		this.language = language;
	}

	public GenderArchetype getMale() {
		return male;
	}

	public void setMale(final GenderArchetype male) {
		this.male = male;
	}

	public GenderArchetype getFemale() {
		return female;
	}

	public void setFemale(final GenderArchetype female) {
		this.female = female;
	}

	public List<String> getWords() {
		return words;
	}

	public void setWords(final List<String> words) {
		this.words = words;
	}

}