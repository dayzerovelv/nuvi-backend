package pt.velv.galpexpand.model.archetype;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

@Embedded
public class GenderArchetype implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 6301302379629379967L;
	
	@Property("description")
	private String description;
	
	@Property("image")
	private String image;
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(final String description) {
		this.description = description;
	}
	
	public String getImage() {
		return image;
	}
	
	public void setImage(final String image) {
		this.image = image;
	}
	
}
