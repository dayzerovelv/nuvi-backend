package pt.velv.galpexpand.model;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

public class BaseDomainObject implements Serializable{

	private static final long serialVersionUID = 5469554100572105634L;

	@Id
	@Property("id")
	private ObjectId id;

	public BaseDomainObject() {
		super();
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

}