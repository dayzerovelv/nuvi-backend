package pt.velv.galpexpand.model.user;

import java.io.Serializable;
import java.util.List;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

import pt.velv.galpexpand.model.BaseDomainObject;

@Embedded
public class UserResults implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -718939384534607773L;
	
	@Property("name")
	private String name;
	
	@Property("description")
	private String description;
	
	@Property("template")
	private String template;
	
	@Property("words")
	private List<String> words;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public List<String> getWords() {
		return words;
	}

	public void setWords(List<String> words) {
		this.words = words;
	}

}
