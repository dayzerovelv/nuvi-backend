package pt.velv.galpexpand.model.user;

import java.io.Serializable;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import pt.velv.galpexpand.model.archetype.ArchetypeResult;

@Entity(value = "surveyUsers", noClassnameStored = true)
public class UserData implements Serializable {

	private static final long serialVersionUID = 5792123263994529127L;

	@Id
	ObjectId objectId;
	
	@Property("user")
	private String user;

	@Property("gender")
	private String gender;

	@Property("companyId")
	private String companyId;

	@Property("language")
	private String language;
	
	@Property("token")
	private String token;
	
	@Property("email")
	private String email;

	@Property("status")
	private String status;

	@Embedded
	private List<UserAnswer> answers;

	@Embedded
	private List<UserResults> results;

	@Embedded
	private ArchetypeResult archetypeResult;

	public ObjectId getObjectId() {
		return objectId;
	}

	public void setObjectId(final ObjectId objectId) {
		this.objectId = objectId;
	}
	
	public String getUser() {
		return user;
	}

	public void setUser(final String user) {
		this.user = user;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(final String gender) {
		this.gender = gender;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(final String companyId) {
		this.companyId = companyId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(final String language) {
		this.language = language;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public List<UserAnswer> getAnswers() {
		return answers;
	}

	public void setAnswers(final List<UserAnswer> answers) {
		this.answers = answers;
	}

	public List<UserResults> getResults() {
		return results;
	}

	public void setResults(final List<UserResults> results) {
		this.results = results;
	}

	public ArchetypeResult getArchetypeResult() {
		return archetypeResult;
	}

	public void setArchetypeResult(final ArchetypeResult archetypeResult) {
		this.archetypeResult = archetypeResult;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getToken() {
		return token;
	}

	public void setToken(final String token) {
		this.token = token;
	}	
	
}
