package pt.velv.galpexpand.model.survey;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

@Embedded
public class GenderData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8093821077974845424L;
	
	@Property("label")
	private String label;
	
	@Property("male")
	private String male;
	
	@Property("female")
	private String female;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getMale() {
		return male;
	}

	public void setMale(String male) {
		this.male = male;
	}

	public String getFemale() {
		return female;
	}

	public void setFemale(String female) {
		this.female = female;
	}

	
}
