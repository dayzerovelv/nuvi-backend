package pt.velv.galpexpand.model.user;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

@Embedded
public class UserAnswer implements Serializable {

	private static final long serialVersionUID = -3679101326737093833L;

	@Property("step")
	private int step;

	@Embedded
	private UserOption option;

	public int getStep() {
		return step;
	}

	public void setStep(final int step) {
		this.step = step;
	}

	public UserOption getOption() {
		return option;
	}

	public void setOption(final UserOption option) {
		this.option = option;
	}

}
