package pt.velv.galpexpand.model.archetype;

import java.util.List;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

@Embedded
public class ArchetypeResult {

	@Property("name")
	private String name;

	@Property("template")
	private String template;

	@Property("description")
	private String description;

	@Property("image")
	private String image;

	@Property("words")
	private List<String> words;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(final String template) {
		this.template = template;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(final String image) {
		this.image = image;
	}

	public List<String> getWords() {
		return words;
	}

	public void setWords(final List<String> words) {
		this.words = words;
	}

}
