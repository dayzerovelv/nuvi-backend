package pt.velv.galpexpand.model.survey;

import java.io.Serializable;
import java.util.List;

import org.mongodb.morphia.annotations.Embedded;

import pt.velv.galpexpand.model.survey.questions.SurveyQuestion;

@Embedded
public class SurveyData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6093037677793486787L;

	@Embedded
	private SurveyIntroData intro;

	@Embedded
	private ProgressBarData progressBar;

	@Embedded
	private GenderData gender;

	@Embedded
	private CommonData common;

	@Embedded
	private List<SurveyQuestion> questions;

	public SurveyIntroData getIntro() {
		return intro;
	}

	public void setIntro(final SurveyIntroData intro) {
		this.intro = intro;
	}

	public ProgressBarData getProgressBar() {
		return progressBar;
	}

	public void setProgressBar(final ProgressBarData progressBar) {
		this.progressBar = progressBar;
	}

	public GenderData getGender() {
		return gender;
	}

	public void setGender(final GenderData gender) {
		this.gender = gender;
	}

	public CommonData getCommon() {
		return common;
	}

	public void setCommon(final CommonData common) {
		this.common = common;
	}

	public List<SurveyQuestion> getQuestions() {
		return questions;
	}

	public void setQuestions(final List<SurveyQuestion> questions) {
		this.questions = questions;
	}

	
	
}
