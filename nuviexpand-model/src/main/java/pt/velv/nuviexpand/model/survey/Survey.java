package pt.velv.galpexpand.model.survey;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

@Entity(value = "surveyData", noClassnameStored = true)
public class Survey implements Serializable {

	private static final long serialVersionUID = -6020217525816249594L;

	@Id
	ObjectId objectId;

	@Property("surveyId")
	private String surveyId;

	@Property("companyId")
	private String companyId;

	@Property("language")
	private String language;

	@Property("token")
	private String token;

	@Embedded
	private SurveyData data;

	public ObjectId getObjectId() {
		return objectId;
	}

	public void setObjectId(final ObjectId objectId) {
		this.objectId = objectId;
	}

	public String getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(final String surveyId) {
		this.surveyId = surveyId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(final String companyId) {
		this.companyId = companyId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(final String language) {
		this.language = language;
	}

	public String getToken() {
		return token;
	}

	public void setToken(final String token) {
		this.token = token;
	}

	public SurveyData getData() {
		return data;
	}

	public void setData(final SurveyData data) {
		this.data = data;
	}

}
