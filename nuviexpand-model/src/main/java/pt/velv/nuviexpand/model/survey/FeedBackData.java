package pt.velv.galpexpand.model.survey;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Property;

public class FeedBackData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8247790127000608770L;
	
	@Property("company")
	private String company;
	
	@Property("text")
	private String text;
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	
}
