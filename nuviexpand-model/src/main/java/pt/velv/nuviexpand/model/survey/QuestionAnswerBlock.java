package pt.velv.galpexpand.model.survey;

import java.io.Serializable;
import java.util.List;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

@Embedded
public class QuestionAnswerBlock implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6886530662430899096L;


	@Property("words")
	private List<String> words;
	
	@Property("image")
	private String image;
	
	@Property("name")
	private String name;
	
	@Property("animation")
	private String animation;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getAnimation() {
		return animation;
	}

	public void setAnimation(final String animation) {
		this.animation = animation;
	}

	public List<String> getWords() {
		return words;
	}

	public void setWords(final List<String> words) {
		this.words = words;
	}

	public String getImage() {
		return image;
	}

	public void setImage(final String image) {
		this.image = image;
	}
	
	
}
