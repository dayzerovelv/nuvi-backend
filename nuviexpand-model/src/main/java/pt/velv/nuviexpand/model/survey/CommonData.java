package pt.velv.galpexpand.model.survey;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

@Embedded
public class CommonData implements Serializable{

	private static final long serialVersionUID = -7784564313772533815L;
	
	@Property("selectOption")
	private String selectOption;
	
	@Property("scroll")
	private String scroll;
	
	@Property("barIceBreaker")
	private String barIceBreaker;
	
	@Property("resultsLabel")
	private String resultsLabel;

	public String getSelectOption() {
		return selectOption;
	}

	public void setSelectOption(String selectOption) {
		this.selectOption = selectOption;
	}

	public String getScroll() {
		return scroll;
	}

	public void setScroll(String scroll) {
		this.scroll = scroll;
	}

	public String getBarIceBreaker() {
		return barIceBreaker;
	}

	public void setBarIceBreaker(String barIceBreaker) {
		this.barIceBreaker = barIceBreaker;
	}

	public String getResultsLabel() {
		return resultsLabel;
	}

	public void setResultsLabel(String resultsLabel) {
		this.resultsLabel = resultsLabel;
	}
	
	
}
