package pt.velv.galpexpand.model.survey;

import java.io.Serializable;
import java.util.List;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

@Embedded
public class SurveyIntroData implements Serializable {

	private static final long serialVersionUID = -2838661114281021035L;

	@Property("phrase1")
	private String phrase1;

	@Property("phrase2")
	private String phrase2;

	@Property("company")
	private String company;

	@Property("messages")
	private List<String> messages;

	@Property("counterText")
	private String counterText;

	@Property("time")
	private String time;

	public String getPhrase1() {
		return phrase1;
	}

	public void setPhrase1(final String phrase1) {
		this.phrase1 = phrase1;
	}

	public String getPhrase2() {
		return phrase2;
	}

	public void setPhrase2(final String phrase2) {
		this.phrase2 = phrase2;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(final String company) {
		this.company = company;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(final List<String> messages) {
		this.messages = messages;
	}

	public String getCounterText() {
		return counterText;
	}

	public void setCounterText(final String counterText) {
		this.counterText = counterText;
	}

	public String getTime() {
		return time;
	}

	public void setTime(final String time) {
		this.time = time;
	}

	
}
