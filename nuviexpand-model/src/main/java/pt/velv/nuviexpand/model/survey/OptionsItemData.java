package pt.velv.galpexpand.model.survey;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Property;

public class OptionsItemData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6716203827597217016L;
	
	@Property("message")
	private String message;
	
	@Property("label")
	private String label;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	
}
