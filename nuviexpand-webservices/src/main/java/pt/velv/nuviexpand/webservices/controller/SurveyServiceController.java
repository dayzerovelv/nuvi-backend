package pt.velv.galpexpand.webservices.controller;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pt.velv.galpexpand.mail.MailService;
import pt.velv.galpexpand.mail.template.ResultEmailTemplate;
import pt.velv.galpexpand.model.archetype.ArchetypeData;
import pt.velv.galpexpand.model.archetype.ArchetypeResult;
import pt.velv.galpexpand.model.survey.Survey;
import pt.velv.galpexpand.model.user.UserData;
import pt.velv.galpexpand.model.user.UserOption;
import pt.velv.galpexpand.service.SurveyService;
import pt.velv.galpexpand.service.UserService;

@RestController
public class SurveyServiceController {

	private Logger logger = LoggerFactory.getLogger(SurveyServiceController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private SurveyService surveyService;

	@Autowired
	private ResourceLoader resourceLoader;

	/**
	 * @Description: Survey REST Service
	 * @param language
	 * @param surveyId
	 * @param userId
	 * @return
	 */
	@GetMapping(value = "/survey/{lang}/{surveyId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getSurveyData(@PathVariable("lang") final String language,
			@PathVariable("surveyId") final String surveyId, @RequestParam("token") final String userToken) {

		UserData userData = userService.getUserData(userToken, surveyId);

		if (userData == null) {
			logger.error("[WEBSERVICES] :: User data not found for token {}", userToken);
			return ResponseEntity.notFound().build();
		}

		if (userData.getStatus() != null && userData.getStatus().equals("COMPLETE")) {
			logger.info("[WEBSERVICES] :: Returning archtype for token {} with completed Survey", userToken);
			return ResponseEntity.ok(userData);
		}

		Survey survey = surveyService.getUserSurveyWithCompany(surveyId, language);

		if (survey == null) {
			logger.error("[WEBSERVICES] :: Survey for {} with language {} not found", surveyId, language);
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(survey);
	}

	/**
	 * @Description: User REST Service
	 * @param userId
	 * @param companyId
	 * @return
	 */
	@GetMapping(value = "/userdata/{companyId}/{userId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getUserData(@PathVariable("userId") final String userId,
			@PathVariable("companyId") final String companyId) {

		logger.info("[WEBSERVICES - userData] :: Received request at userdata for user {} with company {}", userId,
				companyId);

		UserData userData = userService.getUserData(userId, companyId);

		if (userData != null) {
			return ResponseEntity.ok(userData);
		}

		return ResponseEntity.notFound().build();
	}

	/**
	 * 
	 * @param userData
	 * @return
	 */
	@PostMapping(value = "/userdata/save", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> saveUserData(@RequestBody final UserData userData) {
		
		UserData dbUserData = userService.getUserData(userData.getToken(), userData.getCompanyId());
		dbUserData.setAnswers(userData.getAnswers());
		dbUserData.setStatus("COMPLETE");
		dbUserData.setGender(userData.getGender());

		Map<String, Long> collect = userData.getAnswers().stream().map(a -> a.getOption())
				.collect(Collectors.groupingBy(UserOption::getArchetype, Collectors.counting()));

		long maxVal = 0;
		Entry<String, Long> resultArchetype = null;
		for (Entry<String, Long> key : collect.entrySet()) {
			if (key.getValue() >= maxVal) {
				if(key.getValue() == maxVal) {
					ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Draw is not valid");
				}
				resultArchetype = key;
				maxVal = key.getValue();
			}
		}

		ArchetypeData archetypeData = userService.getArcheTypeData(userData.getLanguage(), resultArchetype.getKey());

		if (archetypeData == null) {
			logger.error("Archetype Data Not found for language {} and type {}", userData.getLanguage(),
					resultArchetype);
			ResponseEntity.notFound();
		}

		ArchetypeResult result = new ArchetypeResult();
		result.setName(archetypeData.getName());
		result.setTemplate(archetypeData.getTemplate());

		if (userData.getGender().toLowerCase().equals("m")) {
			result.setDescription(archetypeData.getMale().getDescription());
			result.setImage(archetypeData.getMale().getImage());
		} else {
			result.setDescription(archetypeData.getFemale().getDescription());
			result.setImage(archetypeData.getFemale().getImage());
		}
		result.setWords(archetypeData.getWords());

		dbUserData.setArchetypeResult(result);

		if(userData.getToken().equals("123456")) {
			MailService mailService = new MailService();
			Resource pdfFileResource = resourceLoader
					.getResource("classpath:archetypes/" + result.getName().toUpperCase() + "_" + dbUserData.getLanguage()
							+ "_" + dbUserData.getGender() + ".pdf");
			Resource galpExpand_miniIMG = resourceLoader.getResource("classpath:galpExpand_mini.png");
			Resource galpExpand_smallIMG = resourceLoader.getResource("classpath:galpExpand_small.png");
			Resource learningatGalp_miniIMG = resourceLoader.getResource("classpath:learningatGalp_mini.png");
			ResultEmailTemplate resultTemplate = new ResultEmailTemplate();
			try {
				resultTemplate.setPdfFile(pdfFileResource.getFile());
				resultTemplate.setGalpExpand_miniIMG(galpExpand_miniIMG.getFile());
				resultTemplate.setGalpExpand_smallIMG(galpExpand_smallIMG.getFile());
				resultTemplate.setLearningatGalp_miniIMG(learningatGalp_miniIMG.getFile());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			mailService.sendMail(dbUserData.getEmail(), dbUserData.getUser(), resultTemplate);
			return ResponseEntity.ok(dbUserData);
		}
		
		if (userService.saveUserData(dbUserData)) {
			MailService mailService = new MailService();
			Resource pdfFileResource = resourceLoader
					.getResource("classpath:archetypes/" + result.getName().toUpperCase() + "_" + dbUserData.getLanguage()
							+ "_" + dbUserData.getGender() + ".pdf");
			Resource galpExpand_miniIMG = resourceLoader.getResource("classpath:galpExpand_mini.png");
			Resource galpExpand_smallIMG = resourceLoader.getResource("classpath:galpExpand_small.png");
			Resource learningatGalp_miniIMG = resourceLoader.getResource("classpath:learningatGalp_mini.png");
			ResultEmailTemplate resultTemplate = new ResultEmailTemplate();
			try {
				resultTemplate.setPdfFile(pdfFileResource.getFile());
				resultTemplate.setGalpExpand_miniIMG(galpExpand_miniIMG.getFile());
				resultTemplate.setGalpExpand_smallIMG(galpExpand_smallIMG.getFile());
				resultTemplate.setLearningatGalp_miniIMG(learningatGalp_miniIMG.getFile());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			mailService.sendMail(dbUserData.getEmail(), dbUserData.getUser(), resultTemplate);
			return ResponseEntity.ok(dbUserData);

		}

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}
}
