package pt.velv.galpexpand.mail;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import pt.velv.galpexpand.mail.template.CallOutEmailTemplate;
import pt.velv.galpexpand.mail.template.ResultEmailTemplate;

public class MailSender {

	public void sendMail(final Session session, final String mailFrom, final String mailTo, final String userName, final CallOutEmailTemplate template) {
		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(mailFrom));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailTo));
			message.setSubject(template.getMailSubject());
			message.setContent(template.getEmailTemplate(userName));
			Transport.send(message);

			System.out.println("Start Email sent to:" + mailTo);

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public void sendMail(final Session session, final String mailFrom, final String mailTo, final String userName,final ResultEmailTemplate resultTemplate) {
		try {
		
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(mailFrom));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailTo));
			message.setSubject(resultTemplate.getMailSubject());
			message.setContent(resultTemplate.getEmailTemplate(userName));
			Transport.send(message);

			System.out.println("Completion Email sent to:" + mailTo);

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}