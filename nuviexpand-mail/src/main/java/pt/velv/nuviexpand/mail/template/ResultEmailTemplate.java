package pt.velv.galpexpand.mail.template;

import java.io.File;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

public class ResultEmailTemplate {

	public String mailSubject = "Resultado da Survey";

	private File galpExpand_miniIMG;
	private File learningatGalp_miniIMG;
	private File galpExpand_smallIMG;

	private File pdfFile;

	public Multipart getEmailTemplate(final String userName) {

		String mailData = "<!DOCTYPE html>\r\n" + "<html lang=\"en\">\r\n" + "\r\n" + "<head>\r\n"
				+ "    <meta charset=\"UTF-8\">\r\n"
				+ "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n"
				+ "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\r\n"
				+ "    <title>Email Galp </title>\r\n" + "</head>\r\n" + "\r\n" + "<style>\r\n" + "    body {\r\n"
				+ "        margin: 0;\r\n" + "        font-family: Arial, Helvetica, sans-serif;\r\n" + "    }\r\n"
				+ "</style>\r\n" + "\r\n" + "<body>\r\n"
				+ "    <table style=\"margin: 30px auto; max-width: 800px;\" align=\"center\">\r\n" + "        <tr>\r\n"
				+ "            <td colspan=\"3\" style=\"text-align: center;\">\r\n"
				+ "                <img src=\"cid:learnAtGlpIMG_HEADER\" alt=\"Galp Expand\" width=\"300\">\r\n"
				+ "            </td>\r\n" + "        </tr>\r\n" + "        <tr>\r\n"
				+ "            <td width=\"80\"></td>\r\n" + "            <td style=\"text-align: left;\">\r\n"
				+ "                <br>\r\n" + "                <br><strong>Olá, " + userName.toString()
				+ "</strong>\r\n" + "                <br>\r\n"
				+ "                <br> No documento em anexo, vai encontrar o resultado do questionário que realizou.\r\n"
				+ "                <br> Nele, poderá conhecer-se melhor e potenciar a sua aprendizagem consoante as suas preferências naturais.\r\n"
				+ "                <br> Damos-lhe ainda algumas dicas para que possa “aprender a aprender” em diferentes contextos.\r\n"
				+ "                <br> Agora que sabemos como prefere adquirir conhecimento, vamos poder adequar e personalizar um plano de desenvolvimento\r\n"
				+ "                pensado em si.\r\n" + "                <br>\r\n"
				+ "                <br> Obrigado.\r\n" + "                <br>\r\n"
				+ "                <br><strong>Aceita o desafio?</strong> \r\n" + "                <br>\r\n"
				+ "                <br>\r\n" + "\r\n" + "            </td>\r\n"
				+ "            <td width=\"80\"></td>\r\n" + "        </tr>\r\n" + "        <tr>\r\n"
				+ "            <td colspan=\"3\" style=\"text-align: center;\">\r\n"
				+ "                <img src=\"cid:learnAtGlpIMG_Footer\" alt=\"learning@galp\" width=\"200\">\r\n"
				+ "            </td>\r\n" + "        </tr>\r\n" + "    </table>\r\n" + "</body>\r\n" + "\r\n"
				+ "</html>";

		Multipart multipart = new MimeMultipart("MultiPart");
		MimeBodyPart htmltext = new MimeBodyPart();

		try {
			htmltext.setContent(mailData, "text/html; charset=UTF-8");
			
			String galpExpandLogoSmall = "learnAtGlpIMG_HEADER";
			MimeBodyPart galpExpandTitleSmall = new MimeBodyPart();
			galpExpandTitleSmall.setContentID("<" + galpExpandLogoSmall + ">");
			galpExpandTitleSmall.setDisposition(MimeBodyPart.INLINE);
			galpExpandTitleSmall.attachFile(galpExpand_smallIMG);


			String cidLrngAtGlpFooter = "learnAtGlpIMG_Footer";
			MimeBodyPart learningAtGalpImageFooter = new MimeBodyPart();
			learningAtGalpImageFooter.setContentID("<" + cidLrngAtGlpFooter + ">");
			learningAtGalpImageFooter.setDisposition(MimeBodyPart.INLINE);
			learningAtGalpImageFooter.attachFile(learningatGalp_miniIMG);


			String cidGexpandFooter = "gExpandIMG_Footer";
			MimeBodyPart galpExpandImageFooter = new MimeBodyPart();
			galpExpandImageFooter.setContentID("<" + cidGexpandFooter + ">");
			galpExpandImageFooter.setDisposition(MimeBodyPart.INLINE);
			galpExpandImageFooter.attachFile(galpExpand_miniIMG);


			multipart.addBodyPart(htmltext);
			multipart.addBodyPart(galpExpandTitleSmall);
			multipart.addBodyPart(learningAtGalpImageFooter);
			multipart.addBodyPart(galpExpandImageFooter);

			MimeBodyPart archetypePDF = new MimeBodyPart();
			archetypePDF.attachFile(pdfFile);
			multipart.addBodyPart(archetypePDF);
			
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return multipart;

	}


	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(final String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public File getGalpExpand_miniIMG() {
		return galpExpand_miniIMG;
	}

	public void setGalpExpand_miniIMG(final File galpExpand_miniIMG) {
		this.galpExpand_miniIMG = galpExpand_miniIMG;
	}

	public File getLearningatGalp_miniIMG() {
		return learningatGalp_miniIMG;
	}

	public void setLearningatGalp_miniIMG(final File learningatGalp_miniIMG) {
		this.learningatGalp_miniIMG = learningatGalp_miniIMG;
	}

	public File getGalpExpand_smallIMG() {
		return galpExpand_smallIMG;
	}

	public void setGalpExpand_smallIMG(final File galpExpand_smallIMG) {
		this.galpExpand_smallIMG = galpExpand_smallIMG;
	}

	public File getPdfFile() {
		return pdfFile;
	}

	public void setPdfFile(final File pdfFile) {
		this.pdfFile = pdfFile;
	}

}
