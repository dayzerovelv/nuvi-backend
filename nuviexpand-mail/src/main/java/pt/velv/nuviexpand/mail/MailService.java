package pt.velv.galpexpand.mail;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import pt.velv.galpexpand.mail.template.CallOutEmailTemplate;
import pt.velv.galpexpand.mail.template.ResultEmailTemplate;

public class MailService {

	public boolean sendMail(final String mailTo, final String userName, final CallOutEmailTemplate template) {
		String mailSender = "galptsts@outlook.com";

		// MailSettings
		final String username = mailSender;
		final String password = "8Centos*";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp-mail.outlook.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		MailSender sender = new MailSender();

		sender.sendMail(session, mailSender, mailTo, userName, template);

		return true;
	}

	public boolean sendMail(final String mailTo, final String userName, final ResultEmailTemplate resultTemplate) {

		String mailSender = "galptsts@outlook.com";

		// MailSettings
		final String username = mailSender;
		final String password = "8Centos*";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp-mail.outlook.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		MailSender sender = new MailSender();

		sender.sendMail(session, mailSender, mailTo, userName, resultTemplate);

		return true;
	}

}
