package pt.velv.galpexpand.mail.template;

import java.io.File;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

public class CallOutEmailTemplate {

	public String mailSubject = "Bem-vinda à nova era de aprendizagem!";

	public String token;

	public Multipart getEmailTemplate(final String userName) {

		String mailData = "<!DOCTYPE html>\r\n" + "<html lang=\"en\">\r\n" + "\r\n" + "<head>\r\n"
				+ "    <meta charset=\"UTF-8\">\r\n"
				+ "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n"
				+ "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\r\n"
				+ "    <title>Email Galp </title>\r\n" + "</head>\r\n" + "\r\n" + "<style>\r\n" + "    body {\r\n"
				+ "        margin: 0;\r\n" + "        font-family: Arial, Helvetica, sans-serif;\r\n" + "    }\r\n"
				+ "</style>\r\n" + "\r\n" + "<body>\r\n"
				+ "    <table style=\"margin: 30px auto; max-width: 800px;\" align=\"center\">\r\n" + "        <tr>\r\n"
				+ "            <td colspan=\"3\" style=\"text-align: center;\">\r\n"
				+ "                <img src=\"cid:learnAtGlpIMG_HEADER\" alt=\"Galp Expand\" width=\"300\">\r\n"
				+ "            </td>\r\n" + "        </tr>\r\n" + "        <tr>\r\n"
				+ "            <td colspan=\"3\" style=\"text-align: center;\">\r\n"
				+ "                <h2>Bem-vinda à nova era de aprendizagem!</h2>\r\n" + "            </td>\r\n"
				+ "        </tr>\r\n" + "        <tr>\r\n" + "            <td width=\"80\"></td>\r\n"
				+ "            <td style=\"text-align: left;\">\r\n" + "                <strong>" + userName.toString()
				+ ",</strong>\r\n" + "                <br>\r\n"
				+ "                <br> Sabe como prefere expandir o seu conhecimento?\r\n"
				+ "                <br> Desafiamo-la a descobrir!\r\n" + "                <br>\r\n"
				+ "                <br> Para aceder ao questionário utilize, por favor, o browser Google Chrome.\r\n"
				+ "                <br>\r\n"
				+ "                <br> Link: http://localhost:8080/galpexpand-services/services/survey/EN/GalpExpand?token="
				+ token + "\r\n" + "                <br> Username: diego.kaimoti@dayzero.pt\r\n"
				+ "                <br>\r\n" + "                <br><strong>Aceita o desafio?</strong> \r\n"
				+ "                <br><br>\r\n" + "            </td>\r\n" + "            <td width=\"80\"></td>\r\n"
				+ "        </tr>\r\n" + "        <tr>\r\n"
				+ "            <td colspan=\"3\" style=\"text-align: center;\">\r\n"
				+ "                <img src=\"cid:learnAtGlpIMG_Footer\" alt=\"learning@galp\" width=\"200\">\r\n"
				+ "            </td>\r\n" + "        </tr>\r\n" + "        <tr>\r\n"
				+ "            <td colspan=\"3\" style=\"text-align: center;\">\r\n"
				+ "                <img src=\"cid:gExpandIMG_Footer\" alt=\"Galp Expand\" width=\"300\">\r\n"
				+ "            </td>\r\n" + "        </tr>\r\n" + "    </table>\r\n" + "</body>\r\n" + "\r\n"
				+ "</html>";

		Multipart multipart = new MimeMultipart("MultiPart");
		MimeBodyPart htmltext = new MimeBodyPart();

		try {
			htmltext.setContent(mailData, "text/html; charset=UTF-8");
			String galpExpandLogoSmall = "learnAtGlpIMG_HEADER";
			MimeBodyPart galpExpandTitleSmall = new MimeBodyPart();
			galpExpandTitleSmall.setContentID("<" + galpExpandLogoSmall + ">");
			galpExpandTitleSmall.setDisposition(MimeBodyPart.INLINE);


			galpExpandTitleSmall.attachFile(new File(
					CallOutEmailTemplate.class.getClassLoader().getResource("galpExpand_small.png").getFile()));


			String cidLrngAtGlpFooter = "learnAtGlpIMG_Footer";
			MimeBodyPart learningAtGalpImageFooter = new MimeBodyPart();
			learningAtGalpImageFooter.setContentID("<" + cidLrngAtGlpFooter + ">");
			learningAtGalpImageFooter.setDisposition(MimeBodyPart.INLINE);
//			String learningatGalp_miniURL = "http://34.242.13.53:8080/img/learningatGalp_mini.png";
//			File learningatGalp_miniFile = new File(learningatGalp_miniURL);
//			galpExpandTitleSmall.attachFile(learningatGalp_miniFile);
			learningAtGalpImageFooter.attachFile(new File(
					CallOutEmailTemplate.class.getClassLoader().getResource("learningatGalp_mini.png").getFile()));

			String cidGexpandFooter = "gExpandIMG_Footer";
			MimeBodyPart galpExpandImageFooter = new MimeBodyPart();
			galpExpandImageFooter.setContentID("<" + cidGexpandFooter + ">");
			galpExpandImageFooter.setDisposition(MimeBodyPart.INLINE);
//			String galpExpand_miniURL = "http://34.242.13.53:8080/img/learningatGalp_mini.png";
//			File galpExpand_miniFile = new File(galpExpand_miniURL);
//			galpExpandTitleSmall.attachFile(galpExpand_miniFile);
			galpExpandImageFooter.attachFile(
					new File(CallOutEmailTemplate.class.getClassLoader().getResource("galpExpand_mini.png").getFile()));

			multipart.addBodyPart(htmltext);
			multipart.addBodyPart(galpExpandTitleSmall);
			multipart.addBodyPart(learningAtGalpImageFooter);
			multipart.addBodyPart(galpExpandImageFooter);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return multipart;

	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(final String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getToken() {
		return token;
	}

	public void setToken(final String token) {
		this.token = token;
	}

}
